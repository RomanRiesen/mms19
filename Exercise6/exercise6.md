---
title: Serie 6
lang: de-CH
documentclass: article
papersize: a4paper
fancy: yes
geometry: top=3cm, bottom=3cm, left=3.5cm, right=3.5cm
header-includes: |
    \usepackage{caption}
    \usepackage{subcaption}
    \usepackage{pgf}
    \usepackage{framed}
    \usepackage{tabularx}
---


# Serie 6

## Personas:
Wir kreierten keine konkreten Personas hatten jedoch eine eher junge Demographie im Hinterkopf, die sich wohl fühlt im Umgang mit smartphone typischen Interaktionen (swipen auf Listen-Einträge um diese zu verwalten u.ä.).

## Use cases:
- Benutzer will unverbindlich Häuser anschauen
- Benutzer will Angebote inklusive potentiellen hypotheken Preisen & Raten sehen

## Unser konzept

#### HOUSR Prototype

#### IDEE: vermitteln Immobilien inkl hypothekarlösungsvorschlag an Kunden im tinder style

#### ABLAUF:

- welcome screen- filter prompt ("zuerst präferenzen eingeben" -\> weiter)

- filter: region? zimmerzahl?

badezimmerzahl? wohnfläche? haus oder wohnung?
- Haus-swiping: Bilder vom Haus und 3 buttons like dislike button info (description + relevante infos aus filter)

- finance prompt ("jetzt infos eingeben" -\> weiter)

- finance + info input: vorname + name kontakt email eigenkapital
bruttoeinkommen jährlich =\> klick auf account erstellen- "matches
were made" prompt ("wir haben matches gefunden!" -\> weiter)
falls kein match gefunden: more swiping- matches ansicht (liste) =\> klick auf match

- match detailansicht: mehr infos generell persönliche infos haus (adresse, kosten monat (gängig: 1.2% zins), höhe hypothek kontakt mit für haus verantwortlicher person starten

## STN:

Das STN beinhaltet alles bis das erste mal ein Haus angezeigt wird.
Was fehlt ist die Verwaltung der gelikted Häuser, aber diese ist ziemlich einfach gehalten (siehe Prototyp) und wäre bloss ein weiterer State der das unnötig mit Übergängen gefüllt hätte.

![STN (sorry, dass so hässlich)](./img/stn.jpeg)


## Protoyp:
Es war uns nicht möglich unser gesamtes Konzept in einen Prototypen zu verwandeln.
Insbesondere dynamische aspekte wurden weggelassen da diese sehr, sehr Zeitaufwändig sin in Proto.io und wir uns bereit fühlen würden ein MVP zu starten, wo dies dank Programmierung sehr viel einfacher wäre.




