{\rtf1\ansi\ansicpg1252\cocoartf1671\cocoasubrtf600
{\fonttbl\f0\fnil\fcharset0 Menlo-Bold;\f1\fnil\fcharset0 Menlo-Regular;}
{\colortbl;\red255\green255\blue255;}
{\*\expandedcolortbl;;}
\paperw11900\paperh16840\margl1440\margr1440\vieww14400\viewh7200\viewkind0
\pard\tx566\tx1133\tx1700\tx2267\tx2834\tx3401\tx3968\tx4535\tx5102\tx5669\tx6236\tx6803\pardirnatural\partightenfactor0

\f0\b\fs24 \cf0 \ul \ulc0 HOUSR Prototype
\f1\b0 \ulnone \
\
IDEE:\
vermitteln immobilien inkl hypothekarl\'f6sungsvorschlag an kunden im h\'e4usertinder style\
\
ABLAUF:\
- welcome screen\
- filter prompt ('zuerst pr\'e4ferenzen eingeben' -> weiter)\
- filter:\
	region?\
	zimmerzahl?\
	badezimmerzahl?\
	wohnfl\'e4che?\
	haus oder wohnung?\
- swipe start:\
	bilder haus\
	2 buttons like dislike\
	button info (description + relevante infos aus filter)\
- finance prompt ('jetzt infos eingeben' -> weiter)\
- finance + info input:\
	vorname + name\
	kontakt email\
	eigenkapital\
	bruttoeinkommen j\'e4hrlich\
	=> klick auf account erstellen\
- 'matches were made' prompt ('wir haben matches gefunden!' -> weiter)\
	 falls kein match gefunden: more swiping\
- matches ansicht (liste)\
	=> klick auf match\
- match detailansicht:\
	mehr infos generell\
	pers\'f6nliche infos haus (adresse, kosten monat (g\'e4ngig: 1.2% zins), h\'f6he hypothek\
	kontakt mit f\'fcr haus verantwortlicher person starten\
\
OFFENE FRAGEN:\
- buttons oder swipe?\
- finanzinfo nach test-swiping oder direkt bei filter?\
- wie sieht match-kontaktaufnahme aus? mail aufmachen? anrufen? eigener chat?\
- wie wechselt man zwischen swiping, matches und account?\
- wie kann man unmatchen? (button in matches?)\
\
GEKL\'c4RT:\
- wie sind matches organisiert? \
	=> liste}
