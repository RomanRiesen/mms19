---
title: Serie 3
lang: de-CH
documentclass: article
papersize: a4paper
fancy: yes
geometry: top=3cm, bottom=3cm, left=3.5cm, right=3.5cm
header-includes: |
    \usepackage{caption}
    \usepackage{subcaption}
---
# Serie 3

## 1.)
Bei Apple Music zeigt ein typischer Signifier in Pfeilform an, dass man nach unten swipen kann, um zur Musikliste zu gelangen (und den laufenden Titel zu minimieren). Zur Verdeutlichung wandelt sich der Pfeil beim Herunterziehen zur Linie.\
Im Google Play Store hingegen ist in der Ansicht immer ein Bild der Fotogalerie eines Apps angeschnitten, was ohne expliziten Signifier die Affordance anzeigt, dass geswiped werden kann, um die restlichen Bilder zu sehen. (s. Abb. 1)

![](./img/signifier_example_final.PNG){width=33%}
![](./img/signifier_example_final_2.PNG){width=33%}
![](./img/affordance_example_final.png){width=33%}
\begin{figure}[h!]
\begin{subfigure}[t]{0.66\textwidth}
\caption{Signifier: Apple Music}
\end{subfigure}
\hfill
\begin{subfigure}[t]{0.33\textwidth}
\caption{Affordance: Google Play Store}
\end{subfigure}
\caption{Beispiel Signifier vs. Affordance}
\end{figure}

<!--
![Google Appstore](./img/affordance_example.jpg){height=200px}
![iTunes](./img/signifier_example.jpg){height=200px} DIVISION by 0? FML
-->


## 2.)
Das natürliche Mapping, um nach oben zu scrollen, ist es, nach unten zu ziehen, wie z.B. bei den Endlos-Handtuchdispensern - der Inhalt/Text folgt der Bewegung und wird nach unten verschoben. \
Deshalb ist das natürliche Mapping der Maus-Scroll-Richtung, dass das Finger zu sich ziehen zum Heraufscrollen führt (häufig bei Touchpads). Ein Scrollrad ist meistens entgegen des natürlichen Mappings konfiguriert.

Frage des Standpunkts:\
Wenn Scrollen als Textverschieben (analog Handtuchdispenser) betrachtet wird, ist Touchpad-Scrollen natürliches Mapping und Scrollrad nicht.\
Wenn Scrollen als Verschieben des Anzeigefensters (analog Lupe auf statischem Text) betrachtet wird, ist hingegen Scrollrad natürliches Mapping und die Touchpad-Version des Scrollens nicht.


## 3.)
Das Interlocking (Handlungsablauf folgt festem Muster) bei den meisten Bankomaten beginnt bereits beim ersten Betrachten. Man hat üblicherweise nur die Möglichkeit, einen beliebigen Knopf zu drücken, damit die Bankwerbungsmaske verschwindet. Danach ist das Lock-in (Handlung bleibt aktiv, bis Ablauf fertig abgewickelt) so fest, dass man die Interaktion ohne Entscheidungsverlust als Linie darstellen kann: \
Betrag wählen $\rightarrow$ Karte einfügen $\rightarrow$ Betrag wählen $\rightarrow$ Quittung? $\rightarrow$ Karte herausnehmen $\rightarrow$ Geld (und evtl. Quittung) entnehmen $\rightarrow$ Automat wieder in Warte-Modus \
Das Interessante dabei ist vor allem die Reihenfolge, welche es verunmöglicht, mit Geld, aber ohne Karte vom Automaten weg zu schreiten.
 <!-- vielleicht wäre eine art flussdiagram mit kurzem Kommentar besser geignet da sonst eine endlose Kaskade von 'danach's entsteht. --->


## 4.)
Konzeptmodelle:
: Sind hilfreiche Abstraktionen, die nicht korrekt sein müssen. Diese können sowohl in physischer wie auch in rein mentaler Form existieren.

Mentale Modelle:
: Sind Konzeptmodelle im Verstand der das Artefakt bedienenden Person.

System Image:
: Dies ist die Aggregation allen Wissens bezüglich einer Technologie, welche eine benutzende Person besitzt oder zur Verfügung hat, insbesondere auch die Signifier, Affordances, Constraints und Mappings des Produktes selbst.


Das System Image ist besonders relevant für die Interaktion, da das daraus resultierende mentale Modell beim Benutzen hauptverantwortlich für eine reibungslose Interaktion ist.


## 5.)
- Formulierung des Zieles: Das Ziel ist es, eine Datei von `Document.txt` zu `Pruefungsfragen.txt` umzubenennen.

- Planung der Handlung: Es wäre möglich, in einem Dateimanager in Menus nach der Option 'Umbenennen' zu suchen. Alternativ besteht auch die Möglichkeit, eine Befehlszeile zu öffnen und die Datei per `mv` umzubenennen.

- Spezifikation der Handlungssequenz: Die Variante, per Rechtsklicken auf den Menupunkt `Umbenennen` zu gelangen, wird gewählt.

- Ausführung der Handlungssequenz: Es wird auf die Datei namens `Document.txt` rechtsgeklickt. Dann linksgeklickt, auf dem soeben erschienenen `Umbenennen` im Kontextmenu. Der Schriftzug des Dateinamens leert sich und der Benutzer tippt den neuen Namen `Pruefungsfragen.txt` ein.

- Wahrnehmung: Es erscheint keine Datei mit dem Namen `Document.txt` mehr, jedoch eine mit dem Namen `Pruefungsfragen.txt`, welche angewählt ist.

- Interpretation: Datei wurde umbenannt.

- Vergleich: Das Ziel wurde erreicht.

Die Klüfte, die dabei entstehen:

- Der *Gulf of Execution* der gewählten Variante ist breiter als im Dateimanager auf ein Symbol für `Umbenennen` zu klicken, jedoch schmaler, als wenn eine Kommandozeile geöffnet worden wäre, in der keine Signifiers und nur wenige sicherheitsbedingte Constraints existieren, es kaum ein natürliches Mapping gibt und die einzige Affordance die eigene Erfahrung ist. 

- Der *Gulf of Evaluation* ist in den beiden graphischen Methoden gut überbrückt. Es gibt keine explizite Bestätigung der Veränderung, jedoch bleibt die Datei angewählt und erscheint unter anderem Namen. Der Gulf of Evaluation in der `mv` Methode ist deutlich breiter, da es gar kein Feedback gibt.
<!-- Es isch komisch i- und usgab so heftig müässe z trenne wi i däm modäu...wüu a computer isch iwie inheränt interaktiv! -->

