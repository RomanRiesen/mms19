---
lang: de-CH
documentclass: article
papersize: a4paper
fancy: yes
title: Serie 1
geometry: top=3cm, bottom=3cm, left=3.5cm, right=3.5cm
---
# Serie 1

## 1.)
**Aufgabe:** Hausarbeit schreiben \
Das Werkzeug ist mein Laptop, die Schnittstelle besteht aus einer Tastatur und einem Text-Editor, der Kontext ist ein Tisch in einem ruhigen Uni Gebäude. Ich als Student bin der Benutzer. \
\
**Aufgabe:** Kauf einer Fahrkarte am Automaten \
Das Werkzeug ist ein Fahrkartenautomat, die Schnittstelle ein Touchscreen, der Kontext ist ein belebter Bahnhof. Benutzer ist der Käufer.

## 2.)
Das MEMEX ist eine hypothetische Informations-Verwaltungs-Maschine basierend auf Mikrofilm, welche schnelles Suchen des Gespeicherten ermöglicht und eine Art Hypertext (Assoziation zweier Einträge) implementiert. \
Der Zweck dieser Maschine wäre es, dem Menschen als Gedankenstütze zu dienen. \
\
Der grösste Einfluss von MEMEX ist wohl die Idee von Hypertext-Systemen und deren Traversierung, was den Grundstein gelegt hat für das WWW. Auch die Vorhersage, was die Anwendungen eines solchen Gerätes sein würden - z.B. als Unterstützung bei medizinischen Diagnosen zu dienen - sind überraschend akkurat. \
Spezifisch im Bezug auf die Geschichte von HCI ist wohl die Vorstellung der Möglichkeit einer menschenfreundlicheren Repräsentation der Daten hervorzuheben. [^1]

[^1]: Ähnlich interessant wie das MEMEX finden wir die Cyclops Kamera, 70 Jahre vor der Snapchat-Brille konzipiert.

## 3.)
HCI berührt immer mehr Teile des Lebens, da auch die Computertechnologie immer mehr Teile des Lebens berührt. Somit steigt der Anspruch an HCI, da es immer mehr Kontexte gibt, in denen die Interaktionen reibungslos ablaufen sollen. Auch der Wandel weg vom Büroschreibtisch, wo Effizienz das übergeordnete Ziel ist, hin zu Freizeitanwendungen, wo neue, schwerer - oder gar nicht - messbare Ziele vorhanden sind, führte zu einer Verkomplizierung des Feldes.

## 4.)
Die Verbreitung des Smartphones bedingte die Entwicklung von neuen Interaktionsmustern, da für traditionelle Buttons und Menus schlicht zu viel Platz benötigt wird und die Präzision einer Maus fehlt. \
\
VR und AR Anwendungen bedingen eine kürzere Latenz zwischen Ein- und Ausgabe als traditionelle Echtzeit-Anwendungen, da die Gefahr von Bewegungskrankheit deutlich grösser ist. Die Bedienung von AR Anwendungen, etwa mittels Gestensteuerung, lässt auch noch viele Fragen im Bereich Bedienungsfreundlichkeit und intuitive Steuerung offen.

## 5.)
Wie in anderen Disziplinen ebenfalls ist die Geschichte von Bedeutung in der Kontextualisierung und dem Erschaffen von abstrakten, generell anwendbaren Mustern und Techniken. Dieses Wissen kann verhindern, dass bereits gescheiterte Wege wieder und wieder - ohne signifikante Veränderung - beschritten werden und erlangte Erkenntnisse ignoriert werden, 
wie z.B. das Wissen, dass Displays mit deutlich weniger als 24 Bildern pro Sekunde keine überzeugende Illusion von Bewegung ermöglichen.


