---
title: Serie 5
lang: de-CH
documentclass: article
papersize: a4paper
fancy: yes
geometry: top=3cm, bottom=3cm, left=3.5cm, right=3.5cm
header-includes: |
    \usepackage{caption}
    \usepackage{subcaption}
    \usepackage{pgf}
    \usepackage{framed}
    \usepackage{tabularx}
---

\newcolumntype{Y}{>{\centering\arraybackslash}X}
\newcommand{\ratingBoxes}{\\
    \begin{tabularx}{\textwidth}{l Y Y Y Y Y r}
    Trifft zu & 1 & 2 & 3 & 4 & 5 & Trifft nicht zu
    \end{tabularx}
}

# Serie 5

## 1.)

Die 8 goldenen Regeln von Ben Shneiderman[^reference] in Bezug gebracht mit Nielsen + Molichs Heuristiken lauten:

1. Streben nach Konsistenz:
: *Konsistenz und Standards*

2. Versuche, universelle Benutzbarkeit zu erreichen:
: *Flexibilität und Effizienz der Nutzung*, Shneiderman's Regel geht jedoch noch weiter in Richtung Zugänglichkeit für Menschen mit eingeschränkten Fähigkeiten, Internationalisierung, etc.

3. Biete informative Rückmeldungen:
: Geht über *Sichtbarkeit des Systemstatus* und *Ästhetisches und minimalistisches Design* hinaus, da diese Regel auch das Ausmass des Feedbacks in Abhängigkeit der Aktion anspricht (kurzes, prägnantes Feedback für häufig durchgeführte/kleinere Aktionen, längeres Feedback mit mehr Informationen für seltene/folgenreiche Handlungen).

4. Entwerfe abgeschlossene Dialoge:
: Eine Spezialisierung von *Anwendern helfen, Fehler zu erkennen, zu diagnostizieren und zu beheben* mit Hilfe von *Übereinstimmung zwischen System und der realen Welt* (Papier-Formulare auch auf mehrere Seiten aufgeteilt), *Sichtbarkeit des Systemstatus* (Hervorheben von nicht akzeptierbaren Inputs) und *Ästhetisches und minimalistisches Design*.

5. Verhindere Fehler:
: Entspricht *Fehlervermeidung* und *Anwendern helfen, Fehler zu erkennen, zu diagnostizieren und zu beheben*.

6. Erlaube einfache Umkehrung von Aktionen:
: Fällt unter *Benutzerkontrolle und Freiheit* bei Nielsen + Molich.

7. Lasse dem Benutzer die Kontrolle:
: Hat nicht wirklich ein Äquivalent, höchstens *Sichtbarkeit des Systemstatus*, was Voraussetzung - jedoch nicht hinreichend - ist, um dem Benutzer das Gefühl von Sicherheit (und damit das System unter Kontrolle zu haben) zu geben.

8. Entlaste das Kurzzeitgedächtnis:
: *Erkennen statt Erinnern*

Was bei Shneiderman nicht angesprochen wird, sind *Übereinstimmung zwischen System und der realen Welt* im allgemeinen Fall und nicht im Zusammenhang mit Feedback/Dialogen auftretende *Hilfe und Dokumentation*.

Insgesamt scheinen Shneidermans Regeln im Vergleich mit Nielsen + Molichs Heuristiken auf modernere Anwendungen ausgelegt zu sein. Beispielsweise wird heute - ausserhalb von wenigen Nischen-Programmen - wohl kaum noch jemand in die mitgelieferte Dokumentation schauen, speziell bei mobilen Applikationen. Man verlässt sich eher auf gutes Feedback, Signifiers und intuitives Design.

[^reference]: https://www.cs.umd.edu/~ben/goldenrules.html, 10-Nov-2019


## 2.)

Wenn eine Übereinstimmung zwischen Konventionen oder Gesetzen der echten Welt und dem Artefakt vorhanden sind, so erlaubt dies Deduktion des Verhaltens dessen auf einer breiteren, und dem Benutzer besser bekannten, Basis von Prämissen.\
\
Konsistenz ermöglicht, korrekte Vorhersagen zur Bedienung unbekannter Aspekte des Artefaktes aus der Erfahrung mit diesem zu machen.\
Dies gibt die induktive Schlussfolgerung wieder.\
\
Feedback soll die Verifikation - oder Klarstellung im Falle einer Fehlannahme - von durch Abduktion gewonnenen Implikationsverständnissen ermöglichen.
<!-- I bi mr ni sicher ob dä satz öppis heisst....aber er tönt guet?-->


## 3.)

Use Case Grundsatz 1: Erreichbarkeit\
Ein Kalendereintrag soll in einem bestimmten Zeitabstand wiederholt werden. Die Aufgabe ist elektronisch in konstanter Zeit ausführbar.

- Einen Kalendereintrag wählen
- Hamburger-Icon wählen
- `Wiederholen` wählen
- Zeit-Intervall einstellen
- Ok klicken

\begin{tabularx}{\textwidth}{|l| X|} \hline
Grundsatz & Erreichbarkeit\\ \hline
Use Case & Einen Kalendereintrag wiederholen lassen\\ \hline
Messmethode & Anzahl Interaktionen\\
1st-Level & 2 * \#(mögliche Wiederholungen der Kalendereinträge) \newline
$\implies$ Blättern \& Abschreiben, bis der Kalender endet\\
Schlechtester Fall & 5 Interaktionen\\
Geplantes Level & 5 Interaktionen\\
Bester Fall & 5 Interaktionen\\
\hline \end{tabularx}

Use Case Grundsatz 2: Don't Make Me Think[^reference2]\
Es soll zum heutigen Tag gewechselt werden. Auf modernen elektronischen Kalendern reicht ein Klick auf den Button `Heute`.

\begin{tabularx}{\textwidth}{|l| X|} \hline
Grundsatz & Don't make me think\\ \hline
Use Case & Wechsel zum heutigen Eintrag\\ \hline
Messmethode & Gedanklicher Aufwand\\
1st-Level & Hoch \newline
$\implies$ Zuerst muss das Datum abgeklärt werden, dann zum Tag geblättert und dieser markiert werden\\
Schlechtester Fall & Tief \newline
$\implies$ Button wird nicht auf Anhieb gefunden\\
Geplantes Level & Sehr tief \newline
$\implies$ Es sollte höchstens bis zum Datum gescrollt werden, der heutige Tag ist speziell markiert\\
Bester Fall & Intuitiv \newline
$\implies$ Der Klick auf den Button erfolgt durch Erfahrung unterbewusst\\
\hline \end{tabularx}

[^reference2]: Steve Krug, Don't Make Me Think, Revisited: A Common Sense Approach To Web Usability (2014)


\newpage
## 4.)

Eine Abfolge der Umfrage könnte sein:

1. Nötige demografische und personenspezifische Daten
2. Bisherige Erfahrung mit / Benutzung von Backups
3. Meinung zu Backup-Typen
4. Wünsche für optimalere Backup-Prozesse

Vier mögliche Fragen aus Teil 2:

\begin{framed}
Sie nutzen die Cloud öfter als Backup-Medium als USB-Sticks.
\ratingBoxes

Der Aufwand von Backups hält mich davon ab, diese regelmässiger zu machen.
\ratingBoxes
\end{framed}

\begin{framed}
Haben Sie bereits einen Cloud-Backup Provider ausprobiert?\\
$\square$ Ja \hspace{3cm} $\square$ Nein \\
\\
Falls Sie bei der letzten Frage mit \glqq{}Ja\grqq{} geantwortet haben, welche(n) Provider haben Sie ausprobiert? \\
(mehrere Antworten möglich).\\
$\square$ Dropbox \\
$\square$ Google Drive \\
$\square$ OneDrive \\
$\square$ My Cloud \\
$\square$ Andere: \underline{\hspace{5cm}}
\end{framed}

Eine mögliche Frage aus Teil 3:
\begin{framed}
Cloud-Backups sind zuverlässiger als USB-Stick Backups.
\ratingBoxes
\end{framed}

Ja, wir würden Kontingentfragen benutzen, und zwar zur Spezifizierung von vorherigen Fragen.
Um z.B. herauszufinden, wieso genau jemand eine USB-Stick Präferenz hat - was jedoch nur eine sinnvolle Frage ist, wenn die ausfüllende Person auch eine solche Präferenz hat.

Die Umfrage auf Papier zu verbreiten, könnte durchaus hilfreich dafür sein, dass sie zahlreich beantwortet wird (da persönlicher).


## 5.)
Tagebuchstudien eignen sich, wenn ein Produkt in natürlicher und fast ungestörter Umgebung langzeitig getestet werden soll. Ausserdem erlauben sie tiefere Einblicke in die Handlungsabläufe von Benutzern.\
Falls also in grösseren Projekten bestehende Produkte verbessert oder zukünftige Produkte in Bezug auf ihr Design evaluiert werden sollen, sind Tagebuchstudien durch die detaillierten Informationen und zeitlichen Entwicklungen, die sie liefern, sehr hilfreich.\
Diese Studien sind jedoch aufwändiger als Umfragen oder Interviews und benötigen sehr motivierte Testpersonen.

## 6.)
Personas stellen Benutzergruppen dar. Sie sind nützlich, um Empathie für den Nutzer zu schaffen und ermöglichen so einfachere Kommunkation bezüglich Interface-Design-Entscheidungen. Zu beachten gibt es bei der Erstellung, dass die Persona auf plausiblen Daten basiert, damit man nicht für eine Fantasiefigur designt.

Mindestens definieren sollte eine Persona:

- Soziographische Angaben:
: Bieten Grundlage für weitere Eigenschaften.
- Persönliche Angaben und Merkmale:
: Sind für das Behandeln der Persona als reale Person sehr wichtig.
- Verhaltensmuster / Ziele beim Nutzen der Anwendung:
: Was ist für die Persona relevant? $\implies$ Kosten-Nutzen-Rechnung
- Relevante Vorkenntnisse:
: Damit man sich Herangehensweisen der Persona vorstellen kann.
- Grundlegende Einstellungen:
: Wichtig für die Ausbildung von Empathie, kann aber auch direkteren Einfluss haben, wie z.B. auf die Darstellung von Cookie-Warnungen.
- Angaben zur Umgebung mit Einfluss auf Verhalten:
: Kann einen grossen Einfluss auf das Design und Relevanz für den Benutzer haben.
