---
title: Serie 2
lang: de-CH
documentclass: article
papersize: a4paper
fancy: yes
geometry: top=3cm, bottom=3cm, left=3.5cm, right=3.5cm
---
# Serie 2

## 1.)
Statusleisten wie etwa die Windows Taskbar sollten keine allzu schnell ändernden Elemente - wie etwa eine Uhr mit Sekundenanzeige - enthalten, da diese als ablenkend empfunden werden könnten. Wenn jedoch eine solche schnell veränderliche Anzeige gewünscht ist, sollte diese 2 Modi haben (einen langsam- und einen schnell aktualisierenden/detaillierten), zwischen denen gewechselt werden kann.\

Die Tatsache, dass unser Gehirn Veränderungen in der peripheren Sicht stark wahrnimmt, kann natürlich auch ausgenutzt werden. \
Da besonders Bewegung auf der Fovea wahrgenommen wird, sind etwa die hüpfenden Icons in der macOS Applikationsleiste eine gute Idee zur Kommunikation, welche Anwendungen gerade Aufmerksamkeit benötigen.


## 2.)
Weil das Sehen und das Wahrnehmen getrennter sind, als man sich im Alltag bewusst ist. Es kann per Eye-Tracking in Experimenten nachgewiesen werden, dass Probanden etwas anschauen, sich aber unter den richtigen Umständen (viel sonstige Bewegung im Bild, z.B. durch Kamerafahrt) nicht an das in klarer Sicht verborgene Objekt erinnern können.


<!--
Viel von affordances reden, I guess

Mir kommt nur zu meinem Wecker etwas halbwegs schlaues in den Sinn...Hoffe du hast etwas besseres!
-->
## 3.)
Beispiel: Chord Mojo (DAC/Amplifier Kombination)

Der Mojo hat 3 Knöpfe (Kontrollelemente), welche auch als Anzeigen fungieren, einen LED-Indikator (Anzeige) und Beschriftungen für alle Elemente. Die Anordnung der Elemente ist funktional:

- 2 Knöpfe dienen der Lautstärkeregelung und zeigen die Lautstärke mit einem Farbverlauf über beide Knöpfe an. Der hör- und fühlbare Widerstand beim Klicken macht es einfach zu erkennen, wann die Lautstärke geändert wird. Die Organisation der Knöpfe selbst ist ebenfalls funktional: Der lautstärkesteigernde Knopf ist von der Seite aus gesehen weiter rechts (und somit \'höher\'), der lautstärkesenkende weiter links.

- 1 Knopf fungiert als Power-Button und zeigt als solcher durch einen kurzen Farbwechsel den Bereitschaftsstatus des Geräts an und produziert beim Ein- und Ausschalten jeweils ein hörbares Klacken. Da die Power-Button-Funktion während des Betriebs nicht genutzt wird, wird der Knopf dann zur Darstellung von Zusatzinformationen genutzt (Bitrate der Soundquelle unterscheidbar an der Farbe).

- Der LED-Indikator zur Anzeige des Batteriestatus ist direkt beim Ladeanschluss zu finden und zeigt farblich entweder an, ob das Gerät auflädt, oder wie viel Batterie ungefähr noch verbleibt.

- Auch die In- und Outputs sind funktional angeordnet - auf der einen Seite des Geräts befinden sich die Inputs, auf der anderen Seite die Outputs.

Insgesamt sind die Elemente auch gewissermassen nach Häufigkeit der Verwendung angeordnet: Die Knöpfe, welche häufig verwendet werden und als Anzeigen von wechselnden und deshalb wichtigen Informationen dienen, sind einfach erreich- und sichtbar oben auf der Längsseite des Geräts zu finden; die seltener gebrauchten In- und Outputs sind flach an der Seite angebracht.

![[Chord Mojo](https://chordelectronics.co.uk/product/mojo/)](./img/total_mojo.jpeg){width=100%}

## 4.)
<!-- Hörsymbole für mind. 3 verschiedene Aktionen auf der Oberfläche -->
Beispiele für Hörsymbole mit SonicFinder:

- Beim Entleeren des Papierkorbs wird ein knirschendes Geräusch (ähnlich dem Zerknüllen von Papier) zur Symbolisierung der Zerstörung/Löschung der Dateien abgespielt.

- Während des Kopierens einer Datei ertönt ein Giessgeräusch mit an den Kopierfortschritt angeglichener, ansteigender Frequenz, welches den Duplikationsprozess darstellt und seinen momentanen Status verdeutlicht.

- Das Anwählen eines Objekts (Datei/Ordner/Applikation/etc.) produziert ein Klopfgeräusch, wobei das tonerzeugende Material auf den Objekttyp abgestimmt ist (z.B. Holz für eine Datei, Metall für eine Applikation) und die Frequenz des Geräuschs mit steigender Objektgrösse sinkt. So kann die Art und die Grösse des Objekts beim Selektieren auditorisch kategorisiert werden. Gleichzeitig kann anhand des Klangs teilweise sofort festgestellt werden, dass ein anderes Objekt als das gewünschte ausgewählt wurde. 

## 5.)
<!-- Vorteile von Sound ??? Wer hat denn den Sound überhaupt an?? -->
<!-- mad people, that's who -->
- Es kann zur Erhöhung der Affordance von Benutzerschnittstellen führen, da die Interaktion mittels Maus, Trackpad, Joystick, etc. indirekter ist. Wenn ein Button drückbar aussieht, muss man auf einem Touchscreen nicht zuerst den Mauszeiger finden und ihn über dem zu klickenden Element positionieren, sondern kann ihn direkt - wie einen echten Knopf - mit einem Finger drücken; der Effektor kann natürlicher verwendet werden.

- Auch die Inklusivität der Schnittstelle kann erhöht werden, da ein Touchscreen-Interface leichter angepasst werden kann, z.B. mit grösseren GUI-Elementen für Menschen mit eingeschränkter Sicht oder Motorik. [^1]

- Sound kann ein Indiz für eine Zustandsveränderung sein. Er kann auch dort eingesetzt werden, wo andere Kommunikationswege umständlicher wären, z.B. ein Geräusch beim Verbinden von Bluetooth-Kopfhörern zur Bestätigung der erfolgreichen Verbindungsherstellung.

[^1]: Auch wenn Menschen mit eingeschränkter Sicht wahrscheinlich eine physische Tastatur vorziehen werden.

\newpage
## 6.)

                                SG      KG           LG
---------------------------     ---     ---          ---
Lebenszeit einer Information    0.5s    15s          inf (?)
Abrufzeit                       n/a     70ms         > 100 ms
Kapazität                       n/a     4 Chunks     inf (?)

### (a)
Das sensorische Gedächtnis (SG) dient dazu, visuelle, auditive und haptische Eindrücke zu erhalten, bis diese verarbeitet werden können. Deswegen ist es nicht sinnvoll, von einer Abrufzeit oder der Kapazität zu sprechen. Auch gibt es grosse Variationen in der Lebenszeit von Information zwischen den Teilen des SG; das ikonische Gedächtnis hat eine deutlich kleinere Persistenz als das echoische. \
Das Kurzzeitgedächtnis (KG) hingegen ist eine Art Cache für Denkprozesse; Informationen können kurzzeitig und in begrenztem Umfang gespeichert und abgerufen werden.

### (b)
Das Langzeitgedächtnis (LG) wird unterteilt in das episodische und das semantische Gedächtnis. Ersteres speichert Erlebtes, zweiteres Fakten und erlernte Strukturen.
Es gibt Hinweise darauf, dass beide Teile Informationen deutlich komplexer abbilden können als das KG oder SG. \
Im Gegensatz zum KG können Informationen im LG während langer Zeit und in fast unbegrenzter Kapazität gespeichert werden. Der Zugriff darauf ist allerdings langsamer als beim KG.


## 7.)
### (a)
Erkennen und Erinnern sind zwei verschiedene Arten der Informationsabrufung.\
Erkennen ist die Fähigkeit, Dinge als bekannt wahrzunehmen.\
Erinnern benötigt deutlich mehr Aufwand und besteht aus dem Abruf von Details zu einem Thema.

### (b)
Das Wiedererkennen verbessern. Dies ist z.B. durch die Verwendung von eindeutigen und - wenn möglich - kanonischen Symbolen zu erreichen.

### (c)
Dies macht nicht wirklich Sinn, da das Menu nicht auswendig gelernt werden soll. Das Wiedererkennen sollte reichen, damit man sich orientieren kann. Somit kann das Kurzzeitgedächtnis umgangen werden und es werden keine Chunking-Hilfen benötigt.

## 8.)
Die Validierungs-Nachricht sollte in (visuell) getrennte Blöcke geteilt sein, welche jeweils höchstens vier Zeichen enthalten sollten. Somit sollte es einem Benutzer möglich sein, einen Block zu chunken und diesen einzugeben, ohne dazwischen auf das Zweitgerät schauen zu müssen. \
Da jedoch die Lebenszeiten der Codes relativ kurz gewählt werden können, sollten 4 Zeichen aus Sicht der Sicherheit ausreichen.
