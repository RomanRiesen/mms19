---
title: Serie 4
lang: de-CH
documentclass: article
papersize: a4paper
fancy: yes
geometry: top=3cm, bottom=3cm, left=3.5cm, right=3.5cm
header-includes: |
    \usepackage{caption}
    \usepackage{subcaption}
    \usepackage{pgf}
---
# Serie 4

## 1.)

Vorteile des Sprachparadigmas
:
Insbesondere für Terminal-Anwendungen: 
- Einfachere Wiederholung von Aktionen
- Befehle können kombiniert werden
- Ermöglicht einfacher genauere Kontrolle; eine Gui, die gleich mächtig ist wie eine CLI Applikation mit einigen Dutzend Flags benötigt mindestens ebenso viele Optionen - und dies beinhaltet die oben erwähnten Kombinationen noch nicht. Affordances und Signifiers für eine solche Oberfläche zu gestalten, wäre eine Herausforderung.


Vorteile Aktionsparadigma
:
- Einfacher zugänglich, da mehr Signifier möglich sind und GUIs besser geeignet sind, um beim Erinnern zu helfen $\to$ Einfacherer und wahrscheinlich schnellerer Aufbau eines mentalen Bildes
- Gewisse Interaktionen sind nur mit direktem visuellen Feedback realistisch ausführbar, z.B. Filme schneiden



<!--
Mässig tolls Bispiu iwie...
-->
Visuelle Programmierungen aller Art enthalten Aspekte von beiden Paradigmen.
Man manipuliert Objekte des Sprachparadigmas, z.B. Funktionen mit Eingaben, Ausgaben und Nebeneffekten, aber tut dies mithilfe der Werkzeuge des Aktionsparadigmas.\
Ein spezifisches Beispiel ist die Node-View des Substance Painters, in welcher man die Komposition von Computergrafik-Prozeduren zusammenstellen kann.\

![Substance Painter: Node-View](./img/substance_painter.jpg)



\newpage

## 2.)

Das Diagram sieht so aus:

![Lösung](./img/user_diagram.jpg)

\newpage

## 3.)
![Inkscape STN-Diagram für die Auswahl von Kurven](./img/inkscape_diagram.jpg)

Die Spline Selektion in Inkscape iste eine Quelle der Frustration bei nicht trivialen Änderungen da die Selektion nicht Teil der Undo-History ist.
Dies führt dazu, dass bei einem missklick mehrere Duzend Sekunden Arbeit verloren gehen. Es ist keine leichte Aufgabe zu bestimmen ob ein Klick jetzt auf der einen oder Anderen Kurve landet, was die Selektion an sich manchmal etwas ungenau wirken lässt. Deshalb wäre es besonders wichtig eine Undo-History für Spline-Selektion zu haben.

## 4.)

Ein Ausrutscher betrifft die Ausführung oder Wahrnehmung. Ein Irrtum jedoch das Planen und Vergleichen.
Oder im Interaktionsmodell von Abowd und Beale: Ausrutscher sind eine Fehlübersetzung zwischen Eingabesprache und Kernsprache oder Kernsprache und Ausgabesprache. Irrtümer sind Fehler in den anderen beiden Übersetzungen.

## 5.)

## 6.)
Bei dem Fehler handelt es sich höchstwahrscheinlich um einen Ausrutscher.
Ein solcher Ausrutscher könnte verhindert werden in dem nur Inputs mit einem gewissen Zeitlichen Abstand registriert werden.\
Dazu wäre das hinzufügen von dedizierten `Eingabe` und `Löschen` Knöpfen sicherlich hilfreich.


